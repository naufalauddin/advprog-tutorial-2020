package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
    public Sword() {
        weaponDescription = "Sword do sword sword";
        weaponName = "Sword";
        weaponValue = 25;
    }
}
