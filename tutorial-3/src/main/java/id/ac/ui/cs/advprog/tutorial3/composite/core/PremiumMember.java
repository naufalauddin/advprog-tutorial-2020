package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private boolean isGuildMaster;
    private String name;
    private String role;
    private List<Member> childMembers = new ArrayList<Member>();

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.isGuildMaster = false;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public void setAsGuildMaster() {
        isGuildMaster = true;
    }

    public void dethroneFromGuildMaster() {
        isGuildMaster = false;
    }

    public void addChildMember(Member member) {
        if (isGuildMaster) {
            childMembers.add(member);
        } else {
            if (childMembers.size() < 3) {
                childMembers.add(member);
            } else {
                // do nothing
            }
        }
    }

    public void removeChildMember(Member member) {
        childMembers.remove(member);
    }

    public List<Member> getChildMembers() {
        return childMembers;
    }
}
