package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name;
    private String role;
    private List<Member> childMembers = new ArrayList<Member>();

    public OrdinaryMember(String name, String role) {
        this.name = name;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }
    public void addChildMember(Member member) {
        // do nothing
    }
    public void removeChildMember(Member member) {
        // do nothing
    }
    public List<Member> getChildMembers() {
        return childMembers;
    }
}
