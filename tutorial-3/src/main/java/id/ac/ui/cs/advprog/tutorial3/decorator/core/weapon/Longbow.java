package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    public Longbow() {
        weaponName = "LongBow";
        weaponValue = 15;
        weaponDescription = "Long bow do bow bow";
    }
}
