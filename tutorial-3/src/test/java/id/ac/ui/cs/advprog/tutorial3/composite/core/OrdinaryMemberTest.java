package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Nina", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Merchant", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member nana = new OrdinaryMember("Nana", "Merchant");
        member.addChildMember(nana);
        assertEquals(0, member.getChildMembers().size());
        member.removeChildMember(nana);
        assertEquals(0, member.getChildMembers().size());
    }
}
