package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member asep = new OrdinaryMember("Asep", "Servant");
        member.addChildMember(asep);
        assertEquals(asep, member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member asep = new OrdinaryMember("Asep", "Servant");
        member.addChildMember(asep);
        assertEquals(asep, member.getChildMembers().get(0));
        member.removeChildMember(asep);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member asep = new OrdinaryMember("Asep", "Servant");
        Member doni = new OrdinaryMember("Asep", "Servant");
        Member bambang = new OrdinaryMember("Asep", "Servant");
        Member bimbang = new OrdinaryMember("Asep", "Servant");
        member.addChildMember(asep);
        member.addChildMember(doni);
        member.addChildMember(bambang);
        assertEquals(3, member.getChildMembers().size());
        member.addChildMember(bimbang);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Guild guild = new Guild(member);
        Member asep = new OrdinaryMember("Asep", "Servant");
        Member doni = new OrdinaryMember("Asep", "Servant");
        Member bambang = new OrdinaryMember("Asep", "Servant");
        Member bimbang = new OrdinaryMember("Asep", "Servant");
        member.addChildMember(asep);
        member.addChildMember(doni);
        member.addChildMember(bambang);
        member.addChildMember(bimbang);
        assertEquals(4, member.getChildMembers().size());
    }
}
